Link
=====

* https://github.com/civic/elitech-datareader
* http://www.elitech.uk.com/temperature_logger/Elitech_USB_Temperature_Data_logger_RC_5_147.html
* https://www.amazon.it/dp/B00MQSCZF2

Installazione di ```elitech-datareader```
========================================

```
 pip install elitech-datareader
 sudo usermod -a -G dialout $USER
 
 
 mkdir elitech_RC-5
 cp example.py elitech_RC-5/
 cd elitech_RC-5/
 
```

Comandi
=========

Farsi dare le informazioni di base 
```
python elitech_device.py --command devinfo /dev/ttyUSB0
```

Farsi dare i valori memorizzati
```
python elitech_device.py --command get /dev/ttyUSB0
```

Impostare l'intervallo (in secondi: 900=15 minuti) tra una misurazione e l'altra. ATTENZIONE! cancella i valori memorizzati! e ferma la registrazione!
```
python elitech_device.py --command simple-set --interval=900 /dev/ttyUSB0
```


Impostazione dell'ora esatta

* pare che sia sufficiente collegarlo con il pc (p.es. nel caso di cambio  da ora invernale a ora estiva)


Elenco comandi e opzioni
--------------
```
python elitech_device.py <opzioni> /dev/ttyUSB0
```
Con il comando ```simple-set``` si può mettere un'unica opzione, con il comando ```set``` è possibile mettere più opzioni in una volta. P.es.
```
python elitech-datareader --command set --interval=10  --tone_set=y --alarm=x  /dev/ttyUSB0
```

Non mi è ancora chiaro cosa facciano i comandi ```init``` e ```clock``` e come si usi il comando ```time```.

```
--help     mostra anche altre spiegazioni, che non ho messo qui. 
           Ho omesso quelle che riguardano sia il formato raw che anche la comunicazione con la porta seriale


--command get      resistuisce le temperature memorizzate nel formato : NrProgr YYYY-MM-DD HH:MM:SS  Temp
--command get --page_size=<int>   per RC-5 default=500. NOn so a cosa servi. Pare per debug

--command latest               mostra solo l'ultima misurazione
--command latest --value_only  mostra solo il valore dell'ultima misurazione


--command set --interval=<int>
--command set --upper_limit=<float>
--command set --lower_limit=<float>
--command set --station_no=<int>
--command set --stop_button=< y | n >
--command set --delay=< 0.0 | 0.5 | 1.0 | 1.5 | 2.0 | 2.5 | 3.0 | 3.5 | 4.0 | 4.5 | 5.0 | 5.5 | 6.0 >

--command set --tone_set=< y | n >
--command set --alarm=< x | 3 | 10 >
--command set --temp_unit=< C | F >    Celsius o Fahrenheit
--command set --temp_calibration=<float>
--command set --dev_num=<str>      p.es. 1234567890
--command set --user_info=<str>    mettere un testo libero, evtl tra virgolette
--command set --encode ENCODE       user_info encode



    parser.add_argument('-c', "--command", choices=['init', 'get', 'latest', 'simple-set', 'set', 'devinfo', 'clock', 'raw'])  

    parser.add_argument('--time', type=str)
```



Python
=======

### Get device infomation.

```python
import elitech

device = elitech.Device("/dev/ttyUSB0")

devinfo = device.get_devinfo()
print(devinfo.info)
```
oppure
```python
import elitech

device = elitech.Device("/dev/ttyUSB0")

devinfo = device.get_devinfo()
for k,v in vars(devinfo).items():
    print("{}={}".format(k, v))
```



### Get record data

```python
import elitech

device = elitech.Device("/dev/ttyUSB0")

body = device.get_data()
for elm in body:
    print elm
```
oppure
```python
import elitech

device = elitech.Device("/dev/ttyUSB0")

body = device.get_data()
for elm in body:
    print("{0}\t{1:%Y-%m-%d %H:%M:%S}\t{2:.1f}".format(*elm))
```

### Update param

```python
import elitech

device = elitech.Device("/dev/ttyUSB0")

devinfo = device.get_devinfo()  # leggi parametri attuali.

param_put = devinfo.to_param_put()  #convart devinfo to parameter
param_put.rec_interval = datetime.time(0, 15, 0)    # aggiorna parametro. ora, minuti, secondi
param_put.stop_button = elitech.StopButton.ENABLE   # aggiorna parametro

param_put_res = device.update(param_put)    # update device
```

### Settaggio orario
```python
import elitech
import datetime

device = elitech.Device("/dev/ttyUSB0")

devinfo = device.get_devinfo()  # leggi parametri attuali.
device.set_clock(devinfo.station_no, datetime.datetime.now())
```
